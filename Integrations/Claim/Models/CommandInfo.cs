﻿namespace Claim.Models
{
	public class CommandInfo
	{
		public string Id { get; set; }
		public string CommandName { get; set; }
		public string CommandArgs { get; set; }
		public string ResultApi { get; set; }
		public string SucceededApi { get; set; }
		public string FailedApi { get; set; }
	}
}
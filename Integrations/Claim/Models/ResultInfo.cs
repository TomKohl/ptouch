﻿namespace Claim.Models
{
	public class ResultInfo
	{
		public string Id { get; set; }
		public bool Finished { get; set; }
		public int ExitCode { get; set; }
		public string Output { get; set; }
	}
}
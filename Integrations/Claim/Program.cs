﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Claim.Models;
using Newtonsoft.Json.Linq;

namespace Claim
{
    class Program
    {
        private const string ApiBaseUrl = @"https://pharmatouch.poweredbygnosis.com/api/";
        private const string ApiUser = @"Support@GnosisCloudSolutions.com";
        private const string ApiPassword = @"Supp0rt2017";
        private const string IntegrationTemplateId = "38a0cfcb-c3dc-47e2-9d67-0221c339450b";

        static void Main(string[] args)
        {
            while (true)
            {
                const int seconds = 10;
                var id = Claim(IntegrationTemplateId);

                if (id == null)
                {
                    Thread.Sleep(1000 * seconds);
                    continue;
                }
                
                var command = GetCommand(id);
                Run(command);
            }
        }
        
        public static bool Run(string command)
        {
            var cmd = new CommandInfo
            {
                CommandName = "Sample",
                CommandArgs = command
            };

            var result = new ResultInfo();

            try
            {
                Process app = new Process
                {
                    StartInfo =
                    {
                        FileName = Path.Combine("C:\\Temp", "Sample.exe"),
                        Arguments = "",
                        WorkingDirectory = "",
                        CreateNoWindow = true,
                        UseShellExecute = false,
                        RedirectStandardError = true,
                        RedirectStandardOutput = true
                    }
                };


                app.OutputDataReceived += (sender, e) =>
                {
                    result.Output = (result.Output ?? "") + e.Data;
                };

                app.ErrorDataReceived += (sender, e) =>
                {
                    result.Output = (result.Output ?? "") + e.Data;
                };

                app.Start();
                app.BeginOutputReadLine();
                app.BeginErrorReadLine();

                result.Finished = app.HasExited;
                result.ExitCode = app.HasExited ? app.ExitCode : 0;

                new Thread(() => WaitForProcess(app, cmd, result)).Start();

                return Convert.ToBoolean(result.ExitCode);
            }
            catch (Exception e)
            {
                return false;
            }
        }
        private static void WaitForProcess(Process app, CommandInfo cmd, ResultInfo result)
        {
            app.WaitForExit();

            result.Finished = true;
            result.ExitCode = app.ExitCode;

            Completed(cmd.Id, result.ExitCode == 0);
        }

        private static string GetCommand(string id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(ApiBaseUrl);
                    string authInfo = ApiUser + ":" + ApiPassword;
                    authInfo = Convert.ToBase64String(Encoding.UTF8.GetBytes(authInfo));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authInfo);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = Task.Run(() => client.GetAsync("Item?id=" + id)).Result;

                    var o = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    return (string)o["Command$Display"];
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                return null;
            }
        }

        private static string Claim(string id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(ApiBaseUrl);
                    string authInfo = ApiUser + ":" + ApiPassword;
                    authInfo = Convert.ToBase64String(Encoding.UTF8.GetBytes(authInfo));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authInfo);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    ResultInfo result = new ResultInfo { Id = id };
                    HttpResponseMessage response = Task.Run(() => client.PostAsJsonAsync("Item/" + id + "/Promote/Processing", result)).Result;

                    var o = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    if ((bool)o["success"])
                    {
                        return (string)o["result"]["$AffectedItems"][2]["Id"];
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                return null;
            }
        }

        private static void Completed(string id, bool success = true)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(ApiBaseUrl);
                    string authInfo = ApiUser + ":" + ApiPassword;
                    authInfo = Convert.ToBase64String(Encoding.UTF8.GetBytes(authInfo));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authInfo);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    ResultInfo result = new ResultInfo { Id = id };
                    HttpResponseMessage response = Task.Run(() => client.PostAsJsonAsync("Item/" + id + "/Promote/" + (success ? "Completed" : "Failed"), result)).Result;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
            }
        }

        private static void HandleException(Exception e)
        {
            Trace.WriteLine(e.Message);
            Trace.WriteLine(e.StackTrace);
            Console.WriteLine(e.Message);
            Console.WriteLine(e.StackTrace);
        }
    }
}


﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Http;

namespace Integration.Web.Controllers
{
	//[Authorize]
	public class CommandController : ApiController
	{
		public CommandController()
		{
			var appSettings = WebConfigurationManager.AppSettings;
			string folderPath = appSettings.GetValue("CommandFolder");
			if (!Path.IsPathRooted(folderPath))
			{
				string root = System.Web.Hosting.HostingEnvironment.MapPath("~/");
				folderPath = Path.GetFullPath(Path.Combine(root, folderPath));
			}

			commandFolderPath = folderPath;
		}

		private readonly string commandFolderPath;

        [HttpGet]
        public Models.ResultInfo Run()
        {
            var result = new Models.ResultInfo();
            result.Output = "Got Here!";
            return result;
        }

        [HttpPost]
		public Models.ResultInfo Run(Models.CommandInfo model)
        {
            if (model == null || string.IsNullOrWhiteSpace(model.CommandName))
                HttpCode(System.Net.HttpStatusCode.Conflict, "Command Name is missing.");

            if (model.CommandName.Contains("..") || model.CommandName.Contains(":"))
                HttpCode(System.Net.HttpStatusCode.Conflict, "Invalid Command Name.");

            if (!model.CommandName.EndsWith(".exe"))
                model.CommandName += ".exe";

            var result = new Models.ResultInfo { Id = model.Id };

            try
            {
                Process app = new Process();

                app.StartInfo.FileName = Path.Combine(this.commandFolderPath, model.CommandName);
                app.StartInfo.Arguments = model.CommandArgs;
                app.StartInfo.WorkingDirectory = this.commandFolderPath;
                app.StartInfo.CreateNoWindow = true;
                app.StartInfo.UseShellExecute = false;
                app.StartInfo.RedirectStandardError = true;
                app.StartInfo.RedirectStandardOutput = true;

                app.OutputDataReceived += (sender, e) =>
                {
                    result.Output = (result.Output ?? "") + e.Data;
                };

                app.ErrorDataReceived += (sender, e) =>
                {
                    result.Output = (result.Output ?? "") + e.Data;
                };

                app.Start();
                app.BeginOutputReadLine();
                app.BeginErrorReadLine();

                result.Finished = app.HasExited;
                result.ExitCode = app.HasExited ? app.ExitCode : 0;

                new Thread(() => WaitForProcess(app, model, result)).Start();

                return result;
            }
            catch (Exception e)
            {
                return new Models.ResultInfo { Finished = true, ExitCode = -1, Output = e.Message };
            }
        }

        private void WaitForProcess(Process app, Models.CommandInfo model, Models.ResultInfo result)
		{
			app.WaitForExit();

			result.Finished = true;
			result.ExitCode = app.ExitCode;

			if (!string.IsNullOrEmpty(model.ResultApi))
				SendResult(model.ResultApi, result);

			if (result.ExitCode == 0)
			{
				if (!string.IsNullOrEmpty(model.SucceededApi))
					SendResult(model.SucceededApi, result);
			}
			else
			{
				if (!string.IsNullOrEmpty(model.FailedApi))
					SendResult(model.FailedApi, result);
			}
		}

		private static void SendResult(string url, Models.ResultInfo result)
		{
			try
			{
				var appSettings = WebConfigurationManager.AppSettings;
				string apiBaseUrl = appSettings.GetValue("ApiBaseUrl");
				string apiUser = appSettings.GetValue("ApiUser");
				string apiPassword = appSettings.GetValue("ApiPassword");

				System.Diagnostics.Trace.WriteLine(apiBaseUrl);
				System.Diagnostics.Trace.WriteLine(url);
				Console.WriteLine(url);

				using (HttpClient client = new HttpClient())
				{
					client.BaseAddress = new Uri(apiBaseUrl);

					string authInfo = apiUser + ":" + apiPassword;
					authInfo = Convert.ToBase64String(Encoding.UTF8.GetBytes(authInfo));

					client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authInfo);
					client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

					var response = Task.Run(() => client.PostAsJsonAsync(url, result)).Result;

					response.EnsureSuccessStatusCode();

					Console.WriteLine("OK");
				}
			}
			catch (Exception e)
			{
				System.Diagnostics.Trace.WriteLine(e.Message);
				System.Diagnostics.Trace.WriteLine(e.StackTrace);
				Console.WriteLine(e.Message);
				Console.WriteLine(e.StackTrace);
			}
		}

        [HttpPost]
        private static Models.ResultInfo Next(string id)
        {
            var url = "Item/" + id + "/Promote/Issuing Command";
            try
            {
                var appSettings = WebConfigurationManager.AppSettings;
                string apiBaseUrl = appSettings.GetValue("ApiBaseUrl");
                string apiUser = appSettings.GetValue("ApiUser");
                string apiPassword = appSettings.GetValue("ApiPassword");

                System.Diagnostics.Trace.WriteLine(apiBaseUrl);
                System.Diagnostics.Trace.WriteLine(url);
                Console.WriteLine(url);

                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(apiBaseUrl);

                    string authInfo = apiUser + ":" + apiPassword;
                    authInfo = Convert.ToBase64String(Encoding.UTF8.GetBytes(authInfo));

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authInfo);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var result = new Models.ResultInfo { Id = id };
                    var response = Task.Run(() => client.PostAsJsonAsync(url, result)).Result;
                    response.EnsureSuccessStatusCode();

                    Console.WriteLine(result.Output);

                    return result;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e.Message);
                System.Diagnostics.Trace.WriteLine(e.StackTrace);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return null;
            }
        }

        private static void HttpCode(System.Net.HttpStatusCode code, string message = null)
		{
			var msg = new HttpResponseMessage {
				StatusCode = code,
				Content = new StringContent(message ?? string.Empty)
			};
			throw new HttpResponseException(msg);
		}
	}
}

﻿using System.Web.Http;

namespace Integration.Web
{
	public static class WebApiConfig
	{
		public static void Register(HttpConfiguration config)
		{
			config.Formatters.Remove(config.Formatters.XmlFormatter);

			config.Routes.MapHttpRoute(
			  name: "DefaultApi",
			  routeTemplate: "api/{controller}/{id}",
			  defaults: new { id = RouteParameter.Optional }
			);
		}
	}
}
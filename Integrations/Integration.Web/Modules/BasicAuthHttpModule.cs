﻿using System;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Configuration;

namespace Integration.Web.Modules
{
	public class BasicAuthHttpModule : IHttpModule
	{
		private const string Realm = "Integration.Web";

		public void Dispose()
		{
		}

		public void Init(HttpApplication context)
		{
			context.AuthenticateRequest += OnApplicationAuthenticateRequest;
			context.EndRequest += OnApplicationEndRequest;
		}

		private static void OnApplicationEndRequest(object sender, EventArgs e)
		{
			var response = HttpContext.Current.Response;
			if (response.StatusCode == 401)
			{
				response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", Realm));
			}
		}

		private static void OnApplicationAuthenticateRequest(object sender, EventArgs e)
		{
			var request = HttpContext.Current.Request;
			var authHeader = request.Headers["Authorization"];
			if (authHeader != null)
			{
				var authHeaderVal = AuthenticationHeaderValue.Parse(authHeader);

				// RFC 2617 sec 1.2, "scheme" name is case-insensitive
				if (authHeaderVal.Scheme.Equals("basic", StringComparison.OrdinalIgnoreCase) && authHeaderVal.Parameter != null)
				{
					AuthenticateUser(authHeaderVal.Parameter);
				}
			}
		}

		private static void SetPrincipal(IPrincipal principal)
		{
			Thread.CurrentPrincipal = principal;
			if (HttpContext.Current != null)
			{
				HttpContext.Current.User = principal;
			}
		}

		private static bool AuthenticateUser(string credentials)
		{
			var encoding = Encoding.UTF8;
			credentials = encoding.GetString(Convert.FromBase64String(credentials));

			string[] credentialsArray = credentials.Split(':');
			string username = credentialsArray[0];
			string password = credentialsArray[1];

			var appSettings = WebConfigurationManager.AppSettings;
			if (username != appSettings.GetValue("UserName") || password != appSettings.GetValue("Password"))
				return false;

			IIdentity identity = new GenericIdentity(username);
			SetPrincipal(new GenericPrincipal(identity, null));

			return true;
		}
	}
}
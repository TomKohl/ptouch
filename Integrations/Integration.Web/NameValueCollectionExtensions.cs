﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Integration.Web
{
  internal static class NameValueCollectionExtensions
  {
    public static T GetValue<T>(this NameValueCollection collection, string key)
    {
      var val = collection[key];

      if (string.IsNullOrWhiteSpace(val))
        throw new ValueEmptyException(key);

      var converter = TypeDescriptor.GetConverter(typeof(T));

      return (T)(converter.ConvertFromInvariantString(val));
    }

    public static string GetValue(this NameValueCollection collection, string key)
    {
      var val = collection[key];

      if (string.IsNullOrWhiteSpace(val))
        throw new ValueEmptyException(key);

      return val;
    }
  }

  [Serializable]
  public class ValueEmptyException : Exception
  {
    public ValueEmptyException(string key) : this(key, null)
    {
    }

    public ValueEmptyException(string key, string message) : base(message)
    {
      this.key = key;
    }

    private string key;

    public string Key { get { return this.key; } }

    public override void GetObjectData(SerializationInfo info, StreamingContext context)
    {
      base.GetObjectData(info, context);
      info.AddValue("key", key);
    }
  }
}
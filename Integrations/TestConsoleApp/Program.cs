﻿using System;
using System.Linq;
using System.Threading;

namespace TestConsoleApp
{
	class Program
	{
		static int Main(string[] args)
		{
			// This application just emulates some work. If its command line contains -f, the application will "fail".
			bool shouldFail = args.Contains("-f", StringComparer.OrdinalIgnoreCase);

			Thread.Sleep(5000); // simulate long working

			if (shouldFail)
				Console.Error.WriteLine("The '-f' switch caused failure");
			else
				Console.Out.WriteLine("The command has been completed successfully");

			return shouldFail ? 1 : 0;
		}
	}
}
